﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using LeisureFurn.Models;

namespace LeisureFurn
{
    class Program
    {
        static void Main(string[] args)
        {
            var lineNumber = 0;
            try
            {
                using (StreamReader reader = new StreamReader(@"C:\Users\luyanda\source\repos\data.csv"))
                {
                    Console.WriteLine("Importing.......");
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();

                        var values = line.Split(',');
                        if (lineNumber != 0)
                        {
                            var date = DateTime.ParseExact(values[1], "dd/MM/yyyy hh:mm", null);
                            var exactDate = date.Date;
                            populateDb(values[0], exactDate, values[2], float.Parse(values[3], CultureInfo.InvariantCulture.NumberFormat), values[4], float.Parse(values[5], CultureInfo.InvariantCulture.NumberFormat), float.Parse(values[6], CultureInfo.InvariantCulture.NumberFormat));
                        }

                        lineNumber++;
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex);
            }
            Console.WriteLine("Finished...");
            Console.ReadLine();
        }

        static void populateDb(string InvoiceNumber, DateTime InvoiceDate, string Address, float InvoiceTotal,string description, float Quantity, float UnitSellingPriceExVAT)
        {
            using (var invoice = new FurnContext())
            {
                InvoiceHeader inv = new InvoiceHeader(){ InvoiceNumber = InvoiceNumber, InvoiceDate = InvoiceDate, Address = Address, InvoiceTotal = InvoiceTotal };

                invoice.InvoiceHeaders.Add(inv);

                InvoiceLines invoiceLines = new InvoiceLines() { Description = description, InvoiceHeader = inv, InvoiceNumber = InvoiceNumber, Quantity = Quantity, UnitSellingPriceExVAT = UnitSellingPriceExVAT };
                invoice.InvoiceLineses.Add(invoiceLines);

                invoice.SaveChanges();

                Console.WriteLine("Invoice Number :"+ InvoiceNumber+ " Quantity :"+ Quantity);
            }
        }
    }
}
