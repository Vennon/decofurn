﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LeisureFurn.Models
{
    class InvoiceHeader
    {
        [Key]
        public int InvoiceId { get; set; }
        
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string Address { get; set; }
        public float InvoiceTotal { get; set; }
        public List<InvoiceLines> InvoiceLineses { get; set; }
    }
    class InvoiceLines
    {
        [Key]
        public int LineId { get; set; }
        
        public string InvoiceNumber { get; set; }
        public string Description { get; set; }
        public float Quantity { get; set; }
        public float UnitSellingPriceExVAT { get; set; }
        public InvoiceHeader InvoiceHeader { get; set; }
    }
}
