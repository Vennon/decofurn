﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace LeisureFurn.Models
{
    class FurnContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=.\TRAVELOGIC;Initial Catalog=DecoreFurn;persist security info=False;Integrated Security = false; user=sa; pwd=836666;");
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<InvoiceLines> InvoiceLineses { get; set; }
        public DbSet<InvoiceHeader> InvoiceHeaders { get; set; }
    }
}
