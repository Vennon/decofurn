﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LeisureFurn.Migrations
{
    public partial class initDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InvoiceHeaders",
                columns: table => new
                {
                    InvoiceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InvoiceNumber = table.Column<string>(nullable: true),
                    InvoiceDate = table.Column<DateTime>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    InvoiceTotal = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceHeaders", x => x.InvoiceId);
                });

            migrationBuilder.CreateTable(
                name: "InvoiceLineses",
                columns: table => new
                {
                    LineId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InvoiceNumber = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Quantity = table.Column<float>(nullable: false),
                    UnitSellingPriceExVAT = table.Column<float>(nullable: false),
                    InvoiceHeaderInvoiceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceLineses", x => x.LineId);
                    table.ForeignKey(
                        name: "FK_InvoiceLineses_InvoiceHeaders_InvoiceHeaderInvoiceId",
                        column: x => x.InvoiceHeaderInvoiceId,
                        principalTable: "InvoiceHeaders",
                        principalColumn: "InvoiceId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceLineses_InvoiceHeaderInvoiceId",
                table: "InvoiceLineses",
                column: "InvoiceHeaderInvoiceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InvoiceLineses");

            migrationBuilder.DropTable(
                name: "InvoiceHeaders");
        }
    }
}
